var express = require('express'),
	bodyParser = require('body-parser'),
    exphbs  = require('express-handlebars'),
    session = require('cookie-session');
var app = express();

app.set('views', __dirname + '/views');

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(express.static(__dirname + '/public'));

require('./routes/login.js')(app);
require('./routes/admin.js')(app);
require('./routes/main.js')(app);
require('./routes/user.js')(app);

var server = app.listen(3000, function() {
	console.log("We have started our server on port 3000");
});