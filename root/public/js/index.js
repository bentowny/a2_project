$(document).ready(function() {
	$('#loginButton').show();
	$('#formForLogin').hide();
});

$('#loginButton').click(function() {
	$('.loginText').hide(500, function() {
		$('#loginButton').hide(500);
		$('#formForLogin').show(500);
	});
});