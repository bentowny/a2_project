// initialise view
$(document).ready(function() {
	showHideMain('registerUser', false);
	showHideAll('C1', false);
});


$('#registerUser').click(function() {
	showHideMain('registerUser', true);
});
$('#dateFiller').click(function() {
	showHideMain('dateFiller', true);
});
$('#setBookings').click(function() {
	showHideMain('setBookings', true);
});
$('#authorize').click(function() {
	showHideMain('authorize', true);
});

$('#C1').click(function() {
	showHideAll('C1', true);
});
$('#C2').click(function() {
	showHideAll('C2', true);
});
$('#C3').click(function() {
	showHideAll('C3', true);
});
$('#C4').click(function() {
	showHideAll('C4', true);
});
$('#C5').click(function() {
	showHideAll('C5', true);
});
$('#C6').click(function() {
	showHideAll('C6', true);
});
$('#C7').click(function() {
	showHideAll('C7', true);
});
$('#C8').click(function() {
	showHideAll('C8', true);
});
$('#C9').click(function() {
	showHideAll('C9', true);
});
$('#C10').click(function() {
	showHideAll('C10', true);
});
$('#C11').click(function() {
	showHideAll('C11', true);
});
$('#C12').click(function() {
	showHideAll('C12', true);
});

function showHideMain(option, fade) {
	var options = ['registerUser', 'dateFiller', 'setBookings', 'authorize'];

	for (var i = 0; i < 4; i++) {
		var divID = '#';
			divID += options[i];
			divID += 'Content';	

		if (fade === true) {	
			if (option == options[i]) {
				$(divID).show(500);
				$('#' + options[i]).addClass('selected');
			}
			else {
				$(divID).hide(500);
				$('#' + options[i]).removeClass('selected');
			}
		}
		else {
			if (option == options[i]) {
				$(divID).show();
				$('#' + options[i]).addClass('selected');
			}
			else {
				$(divID).hide();
				$('#' + options[i]).removeClass('selected');
			}
		}
	}
}

function showHideAll(room, fade) {
	var rooms = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12'];

	for (var i = 0; i < 12; i++) {
		var divID = '#';
			divID += rooms[i];
			divID += 'Content';	

		if (fade === true) {	
			if (room == rooms[i]) {
				$(divID).show(500);
				$('#' + rooms[i]).addClass('selected');
			}
			else {
				$(divID).hide(500);
				$('#' + rooms[i]).removeClass('selected');
			}
		}
		else {
			if (room == rooms[i]) {
				$(divID).show();
				$('#' + rooms[i]).addClass('selected');
			}
			else {
				$(divID).hide();
				$('#' + rooms[i]).removeClass('selected');
			}
		}
	}
}