module.exports = function(app) {
	app.get('/', function(req, res) { 
		res.render('index.handlebars', {layout: false, loginMessage:''}); //load the index page
	});
	app.get('/index', function(req,res) {
		res.render('index.handlebars', {layout: false, loginMessage:''}); //load the index page
	});
	app.get('/help', function(req,res) {
		res.render('help.handlebars', {layout: false}); //load the help page
	});

	app.get('*', function(req, res){
	 	res.render('404.handlebars', {layout: false}); //load error page
	});
}