module.exports = function(app) {
	db = require('diskdb');
	db.connect('./db', ['staff', 'days']); //connect to database	

	var encryption = require('./functionality/encryption');
	var createtableview = require('./functionality/createauthorizeview');
	var createsetbookingview = require('./functionality/createsetbookingview');

	app.post('/admin_register', function(req, res) {
		var username = req.body.username,
			password = req.body.password;
		var admin_user = req.body.user,
			admin_password = req.body.pass;	

		var staff = db.staff.find({user:admin_user, pass:admin_password}); //get all the staff members with these login details

		if (staff.length > 0 && staff[0].admin === 1) { //if a staff member exists and is an admin
			var newUserCheck = db.staff.find({user:username});

			if (newUserCheck.length == 0) {
				var encryptedPass = encryption.encrypt(password);
				var newUser = { 
					user:username, 
					pass:encryptedPass,
					admin: 0
				}
				db.staff.save(newUser);

				res.redirect('/admin?username=' + admin_user + '&password=' + admin_password + '&registermessage=' + 'New%20User%20Added');
			}
			else {
				res.redirect('/admin?username=' + admin_user + '&password=' + admin_password + '&registermessage=' + 'The%20User%20Already%20Exists');
			}
		}
		else {
			res.redirect('/admin?username=' + admin_user + '&password=' + admin_password + '&registermessage=' + 'An%20Error%20Occured');
		}
	});

	app.post('/admin_datefiller', function(req, res) {
		var datefiller = require('./functionality/datefiller');
		datefiller.clear();

		var username = req.body.user,
			password = req.body.pass;

		var firstMondayOfTerm = new Date(ChangeDateFormat(req.body.firstMonday)),
			firstHalfTerm = new Date(ChangeDateFormat(req.body.firstHalfTerm)),
			christmas = new Date(ChangeDateFormat(req.body.christmas)),
			secondHalfTerm = new Date(ChangeDateFormat(req.body.secondHalfTerm)),
			easter = new Date(ChangeDateFormat(req.body.easter)),
			thirdHalfTerm = new Date(ChangeDateFormat(req.body.thirdHalfTerm)),
			summer = new Date(ChangeDateFormat(req.body.summer));
		
		datefiller.fill(username, password, firstMondayOfTerm, firstHalfTerm, christmas, secondHalfTerm, easter, thirdHalfTerm, summer);

		var staff = db.staff.find({user:username, pass:password}); //get all the staff members with these login details
		var databaseTables = createtableview.create(username, password);

		var dates = db.days.find();

		if (dates.length > 0) {
			res.redirect('/admin?username=' + username + '&password=' + password + '&datemessage=' + 'Dates%20Added');
		}
		else {
			res.redirect('/admin?username=' + username + '&password=' + password + '&datemessage=' + 'An%20Error%20Occured');	
		}
	});

	app.post('/setBooking', function(req, res) {
		db = require('diskdb');
		db.connect('./db', ['days']);

		var user = req.body.user,
			pass = req.body.pass,
			week = req.body.week,
			period = req.body.period,
			staff = req.body.staff,
			room = req.body.room,
			yearGroup = req.body.yearGroup,
			day = req.body.day;

		var newBooking = {
			room: room,
			staffCode: staff, 
			yearGroup: yearGroup,
			approved: 0,
			setBooking: 1
		}	

		options = {
		    multi: false, // update multiple - default false
			upsert: false // if object is not found, add it (update-insert) - default false
		}

		var existingData = db.days.find({day: week + "-" + day});

		console.log(existingData);

		for (var i = 0; i < existingData.length; i++) {
			if (period == 1) {
				for (var j = 0; j < existingData[i].p1Bookings.length; j++) {
					if (existingData[i].p1Bookings[j].room == room) {
						existingData[i].p1Bookings.splice(j, 1);
						console.log("removing booking form P1");
					}
				}

				existingData[i].p1Bookings.push(newBooking);
				var updated = db.days.update({_id:existingData[i]._id}, existingData[i], options);
				console.log(updated);
			}
			else if (period == 2) {
				for (var j = 0; j < existingData[i].p2Bookings.length; j++) {
					if (existingData[i].p2Bookings[j].room == room) {
						existingData[i].p2Bookings.splice(j, 1);
						console.log("removing booking form P2");
					}
				}

				existingData[i].p2Bookings.push(newBooking);
				var updated = db.days.update({_id:existingData[i]._id}, existingData[i], options);
				console.log(updated);
			}
			else if (period == 3) {
				for (var j = 0; j < existingData[i].p3Bookings.length; j++) {
					if (existingData[i].p3Bookings[j].room == room) {
						existingData[i].p3Bookings.splice(j, 1);
						console.log("removing booking form P3");
					}
				}

				existingData[i].p3Bookings.push(newBooking);
				var updated = db.days.update({_id:existingData[i]._id}, existingData[i], options);
				console.log(updated);
			}
			else if (period == 4) {
				for (var j = 0; j < existingData[i].p4Bookings.length; j++) {
					if (existingData[i].p4Bookings[j].room == room) {
						existingData[i].p4Bookings.splice(j, 1);
						console.log("removing booking form P4");
					}
				}

				existingData[i].p4Bookings.push(newBooking);
				var updated = db.days.update({_id:existingData[i]._id}, existingData[i], options);
				console.log(updated);
			}
			else if (period == 5) {
				for (var j = 0; j < existingData[i].p5Bookings.length; j++) {
					if (existingData[i].p5Bookings[j].room == room) {
						existingData[i].p5Bookings.splice(j, 1);
						console.log("removing booking form P5");
					}
				}

				existingData[i].p5Bookings.push(newBooking);
				var updated = db.days.update({_id:existingData[i]._id}, existingData[i], options);
				console.log(updated);
			}
			else {
				console.log("I did do the breaking");
			}
		}

		/*var updated = db.days.update({day: week + "-" + day}, existingData, options);
		console.log(updated);*/
		
		res.redirect('/admin?username=' + user + '&password=' + pass);
	});

	app.post('/authorizeRoom', function(req, res) {
		db = require('diskdb');
		db.connect('./db', ['days']); //connect to database	

		var user = req.body.user,
			pass = req.body.pass,
			date = req.body.date,
			booking = req.body.bookings,
			period = req.body.period;
			room = req.body.room;

		var day = db.days.find({date:date});
			day = day[0];

		if (period == 1) {
			var thisBooking = day.p1Bookings[booking];
			thisBooking.approved = 1;

			day.p1Bookings.push(thisBooking);

			for (var j = 0; j < day.p1Bookings.length; j++) {
				if (day.p1Bookings[j].room == room) {
					day.p1Bookings.splice(j, 1);
				}
			}

			var updated = db.days.update({date:date}, day);
		}
		else if (period == 2) {
			var thisBooking = day.p2Bookings[booking];
			thisBooking.approved = 1;

			day.p2Bookings.push(thisBooking);

			for (var j = 0; j < day.p2Bookings.length; j++) {
				if (day.p2Bookings[j].room == room) {
					day.p2Bookings.splice(j, 1);
				}
			}

			var updated = db.days.update({date:date}, day);
		}
		else if (period == 3) {
			var thisBooking = day.p3Bookings[booking];
			thisBooking.approved = 1;

			day.p3Bookings.push(thisBooking);

			for (var j = 0; j < day.p3Bookings.length; j++) {
				if (day.p3Bookings[j].room == room) {
					day.p3Bookings.splice(j, 1);
				}
			}

			var updated = db.days.update({date:date}, day);
		}
		else if (period == 4) {
			var thisBooking = day.p4Bookings[booking];
			thisBooking.approved = 1;

			day.p4Bookings.push(thisBooking);

			for (var j = 0; j < day.p4Bookings.length; j++) {
				if (day.p4Bookings[j].room == room) {
					day.p4Bookings.splice(j, 1);
				}
			}

			var updated = db.days.update({date:date}, day);
		}
		else if (period == 5) {
			var thisBooking = day.p5Bookings[booking];
			thisBooking.approved = 1;

			day.p5Bookings.push(thisBooking);

			for (var j = 0; j < day.p5Bookings.length; j++) {
				if (day.p5Bookings[j].room == room) {
					day.p5Bookings.splice(j, 1);
				}
			}

			var updated = db.days.update({date:date}, day);
		}

		//var updated = db.days.update({date:date}, day, options);

		res.redirect('/admin?username=' + user + '&password=' + pass);
	});

	app.get('/encrypt', function(req, res) {
		var password = req.query.password;

		password = encryption.encrypt(password);
		res.send(password);
	});

	function ChangeDateFormat(date) { //convers dd-mm-yyyy to mm-dd-yyyy and visa versa
		if (date.indexOf('/') > -1) { //contains slashes
			var splitDate = date.split('/');
			return [splitDate[1], splitDate[0], splitDate[2]].join('/');
		}
		else if (date.indexOf('-') > -1) { //contains hyphens
			var splitDate = date.split('-');
			return [splitDate[1], splitDate[0], splitDate[2]].join('/');
		}
		else {
			return '';
		}
	}
}