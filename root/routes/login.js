module.exports = function(app) {
	db = require('diskdb');
	db.connect('./db', ['staff']); //connect to database

	var createtableview = require('./functionality/createtableview');
	var createauthorizeview = require('./functionality/createauthorizeview');
	var createsetbookingview = require('./functionality/createsetbookingview');
	var encryption = require('./functionality/encryption');

	app.post('/login', function(req, res) {
		var username = req.body.username,
			password = req.body.password;

		if (password != '') {
			password = encryption.encrypt(password);
		}

		if (username == 'guest' && password == '') {
			var databaseTables = createtableview.create('guest', '');

			res.render('guest.handlebars', {layout: false, user:'guest', pass:'', tables:databaseTables});
		}
		else {
			var staff = db.staff.find({user:username}); //get all the staff members with these login details

			if (staff.length > 0) {
				if (staff[0].admin === 1 && staff[0].pass == password) { //if the staff member is marked as admin
					res.redirect('/admin?username=' + username + '&password=' + password);
				}
				else if (staff[0].admin === 0 && staff[0].pass == password) { //if staff isnt admin
					res.redirect('/user?username=' + username + '&password=' + password);
				}
				else {
					console.log(username);
					console.log(password);
					console.log(staff[0].pass);
					res.render('index.handlebars', {layout: false, loginMessage: 'Something went wrong.'}); //will hopefully never reach here
				}
				
			}
			else {
				res.render('index.handlebars', {layout: false, loginMessage: 'Login details incorrect.'}); //no staff exists with these credentials	
			}
		}
	});

	app.get('/user', function(req, res) {
		var username = req.query.username,
			password = req.query.password;

		var staff = db.staff.find({user:username}); //get all the staff members with these login details

		if (staff.length > 0) {
			if (staff[0].user == username && staff[0].pass == password && staff[0].admin === 0) {
				var databaseTables = createtableview.create(username, password);

				res.render('user.handlebars', {layout: false, user: username, pass: password, tables:databaseTables}); //load user page		
			}
			else {
				res.render('index.handlebars', {layout: false, loginMessage: 'Something went wrong.'}); //will hopefully never reach here
			}
		}
		else {
			res.render('index.handlebars', {layout: false, loginMessage: 'Login details incorrect.'}); //no staff exists with these credentials	
		}
	});

	app.get('/admin', function(req, res) {
		var username = req.query.username,
			password = req.query.password;
			registerMessage = req.query.registermessage;
			dateFillerMessage = req.query.datemessage;

		var staff = db.staff.find({user:username}); //get all the staff members with these login details

		if (staff.length > 0) {
			if (staff[0].user == username && staff[0].pass == password && staff[0].admin === 1) {
				var databaseTables = createauthorizeview.create(username, password);
				var setbookingView = createsetbookingview.create(username, password);

				res.render('admin.handlebars', {layout: false, user: username, pass:password, registerMessage:registerMessage, tables: databaseTables, setbooking:setbookingView, dateFillerMessage:dateFillerMessage}); //load the admin page
			}
			else {
				res.render('index.handlebars', {layout: false, loginMessage: 'Something went wrong.'}); //will hopefully never reach here
			}
		}
		else {
			res.render('index.handlebars', {layout: false, loginMessage: 'Login details incorrect.'}); //no staff exists with these credentials	
		}
	});

	
}