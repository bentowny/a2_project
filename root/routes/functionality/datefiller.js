var datefiller = {};
datefiller.fill = function(username, password, firstMondayOfTerm, firstHalfTerm, christmas, secondHalfTerm, easter, thirdHalfTerm, summer) {
	db = require('diskdb');
	db.connect('./db', ['staff', 'days']); //connect to database

	var noOfWeeks = NoOfWeeksBetween(firstMondayOfTerm, summer) + 1;
	var mondays = [];
	var days = [];

	var oneWeek = 604800000; //1000 * 60 * 60 * 24 * 7
	var oneDay = 86400000; //1000 * 60 * 60 * 24

	for (var i = 0; i < noOfWeeks; i++) { //loops through weeks
		if (i == 0) { //first week
			curDay = new Date(firstMondayOfTerm);
			hours = curDay.getHours();

			//guarentess its at midnight
			if (hours == 23) { //guarentess its at midnight
				curDay = new Date(curDay.getTime() + 60*60000);
			}
			else if (hours == 1) {
				curDay = new Date(curDay.getTime() - 60*60000);
			}

			firstMondayOfTerm = curDay;

			mondays.push(firstMondayOfTerm); //adds to table
		}
		else {
			var previousWeek = mondays[i-1].getTime();

			var curWeek = new Date(previousWeek + oneWeek);
			mondays.push(curWeek); //adds to table
		}
	}

	var newMondays = [];
	for (var i = 0; i < mondays.length; i++) { //loops through all of the Mondays
		var i_monday = mondays[i],
			curDay = new Date(i_monday),
			hours = curDay.getHours(); //gets the hours

		//guarentees its at midnight
		if (hours == 23) {
			curDay = new Date(curDay.getTime() + 60*60000);
		}
		else if (hours == 1) {
			curDay = new Date(curDay.getTime() - 60*60000);
		}

		//removes the holiday Mondays
		if (String(curDay) == String(new Date(firstHalfTerm))) {
			console.log('Skipping a week for October Half Term');
		}
		else if (String(curDay) == String(new Date(secondHalfTerm))) {
			console.log('Skipping a week for Spring Half Term');
		}
		else if (String(curDay) == String(new Date(thirdHalfTerm))) {
			console.log('Skipping a week for Summer Half Term');
		}

		else if (String(curDay) == String(new Date(christmas))) {
			console.log('Skipping a week for Christmas Holiday');
			i++;
		}
		else if (String(curDay) == String(new Date(easter))) {
			console.log('Skipping a week for Easter');
			i++;
		}
		else if (String(curDay) == String(new Date(summer))) {
			console.log('Skipping a week for Summer Holiday');
			i++;
		}		
		else {
			newMondays.push(mondays[i]); //adds to table
		}
	} 

	//creates every day for each Monday
	for (var i = 0; i < newMondays.length; i++) { //loops through all mondays
		var mondayMS = newMondays[i].getTime();
		for (var j = 0; j < 5; j++) { //loops for each day of the week
			var curDay = new Date(mondayMS + (j * oneDay)),
				hours = curDay.getHours();

			if (hours == 23) {
				curDay.addHours(1);
			}

			days.push(curDay); //adds to table
		}
	}

	var week = 1;
	var dayCounter = 0;

	for (var i = 0; i < days.length; i++) {
		var newDay = {
			date: days[i],
			week: week,
			p1Bookings: [],
			p2Bookings: [],
			p3Bookings: [],
			p4Bookings: [],
			p5Bookings: [],
			day: week + "-" + dayCounter
		}

		dayCounter++;
		db.days.save(newDay); //adds to database

		if (dayCounter > 4) { //alternates through the weeks 
			if (week == 1) {
				week++;
			}
			else if (week == 2) {
				week--;
			}

			dayCounter = 0;
		}
	}
}

//custom function to add hours
Date.prototype.addHours= function(h){ 
    this.setHours(this.getHours()+h);
    return this;
}

//clears the database
datefiller.clear = function() {
	db = require('diskdb');
	db.connect('./db', ['staff', 'days']); //connect to database

	db.days.remove({week : '1'}, true);
	db.days.remove({week : '2'}, true);
}
//returns all of the days in the database
datefiller.returnAllDays = function() {
	db = require('diskdb');
	db.connect('./db', ['days']);

	var days = db.days.find();

	for (var i = 0; i < days.length; i++) {
		console.log(new Date(days[i].date).getDay());
	};
}
//returns a specific day
datefiller.returnDay = function(dbValue) {
	var date = new Date(dbValue.date).getDay();

	switch(date) {
		case 1:
			return 'Monday';
			break;
		case 2:
			return 'Tuesday';
			break;
		case 3:
			return 'Wednesday';
			break;
		case 4:
			return 'Thursday';
			break;
		case 5:
			return 'Friday';
			break;
	}
	return '';
}

//finds the number of weeks between two dates
function NoOfWeeksBetween(date1, date2) {
	var oneWeek = 604800000; //1000 * 60 * 60 * 24 * 7

	var date1_ms = date1.getTime(),
		date2_ms = date2.getTime(),
		differenceMS = Math.abs(date1_ms - date2_ms);

	return Math.floor(differenceMS / oneWeek);
}

module.exports = datefiller; //exports as a module so this code can be run by the web app