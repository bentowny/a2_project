var createsetbookingview = {}
createsetbookingview.create = function(username, password) { //create function
	//creates the main layout for week one
	var string = "Week One <br>";

		string += "<table>";

		string += "<tr>";
		string += "<td style='background-color: white'>Day</td>";
		string += "<td style='background-color: white'>Period 1</td>";
		string += "<td style='background-color: white'>Period 2</td>";
		string += "<td style='background-color: white'>Period 3</td>";
		string += "<td style='background-color: white'>Period 4</td>";
		string += "<td style='background-color: white'>Period 5</td>";
		string += "</tr>";

		string += createdayview(1, username, password); //adds the tables using database values

		string += "</table>";

	//creates the main layout for week two
	string += "<br>Week Two<br>"

		string += "<table>";

		string += "<tr>";
		string += "<td style='background-color: white'>Day</td>";
		string += "<td style='background-color: white'>Period 1</td>";
		string += "<td style='background-color: white'>Period 2</td>";
		string += "<td style='background-color: white'>Period 3</td>";
		string += "<td style='background-color: white'>Period 4</td>";
		string += "<td style='background-color: white'>Period 5</td>";
		string += "</tr>";

		string += createdayview(2, username, password); //adds the tables using database values

		string += "</table>";

	return string;	
}

//creates the day view
createdayview = function(week, username, password) {
	var string = '';
	for (var i = 0; i < 5; i++) { //loops through each day of the week
		string += "<tr>";
		string += dayOfTheWeek(i); //adds the day to the table

		for (var j = 0; j < 5; j++) { //loops through each period of the day
			//creates the table element
			string += "<td style='background-color: white'>";
			string += "<form action='/setBooking' method='post'>";
			string += "<input type='hidden' name='user' value=" + username + ">";
			string += "<input type='hidden' name='pass' value=" + password + ">";
			string += "<input type='hidden' name='week' value=" + week + ">";
			string += "<input type='hidden' name='period' value=" + (j+1) + ">";
			string += "<input type='hidden' name='day' value=" + i + ">";
			
			string += optionview;
			string += "<br>";
			string += createyeargroup();
			string += "<br>";
			string += createroomview();
			string += "<br>";
			string += "<input type='submit' value='Book'>";
			string += "</form>"
			string += "</td>";
		}

		string += "</tr>";
	}

	return string; //returns the HTML
}

//creates the staff dropdown
createoptionview = function() {
	db = require('diskdb');
	db.connect('./db', ['staff']); 

	var staff = db.staff.find();

	var string = "<select name='staff'>";

	for (var i = 0; i < staff.length; i++) { //loops through all the members of staff
		if (staff[i].admin == 0) { //ignores admins
			string += "<option value='" + staff[i].user + "'>" + staff[i].user + "</option>";
		}
	}

	string += "</select>";

	return string; //returns option
}

var optionview = createoptionview(); //allows the option view dropdown to be re used

//creates the year group dropdwon
createyeargroup = function() {
	var string = "<select name='yearGroup'>";
		string += "<option value='7'>Year 7</option>";
		string += "<option value='8'>Year 8</option>";
		string += "<option value='9'>Year 9</option>";
		string += "<option value='10'>Year 10</option>";
		string += "<option value='11'>Year 11</option>";
		string += "<option value='12'>Year 12</option>";
		string += "<option value='13'>Year 13</option>";
		string += "</select>";

	return string; //returns the dropdown
}

//creates the room dropdown
createroomview = function() {
	var string = "<select name='room'>";
		string += "<option value='C1'>C1</option>";
		string += "<option value='C2'>C2</option>";
		string += "<option value='C3'>C3</option>";
		string += "<option value='C4'>C4</option>";
		string += "<option value='C5'>C5</option>";
		string += "<option value='C6'>C6</option>";
		string += "<option value='C7'>C7</option>";
		string += "<option value='C8'>C8</option>";
		string += "<option value='C9'>C9</option>";
		string += "<option value='C10'>C10</option>";
		string += "<option value='C11'>C11</option>";
		string += "<option value='C12'>C12</option>";
		string += "</select>";

	return string;	//returns the dropdown
}

//converts a number in the range 0 to 6, to the string of the day
dayOfTheWeek = function(day) {
	var string = "<td style='background-color: white'>";

	switch (day) {
		case 0:
			string += 'Monday';
			break;
		case 1:
			string += 'Tuesday';
			break;
		case 2:
			string += 'Wednesday';
			break;
		case 3:
			string += 'Thursday';
			break;
		case 4:
			string += 'Friday';
			break;
	}

	string += "</td>"

	return string; //returns the relative day
}

module.exports = createsetbookingview; //exports as a module so this code can be run by the web app