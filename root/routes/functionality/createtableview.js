var createtableview = {};
createtableview.create = function(username, password) { //the create table function
	db = require('diskdb');
	db.connect('./db', ['days']); //connects to the database

	var monday = new Date();
		allDays = db.days.find(),
		usefulDays1 = [],
		usefulDays2 = [],
		usefulDays3 = []; //initialises variables

	var day = monday.getDay(),
		diff = monday.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday

	//sets it to be exactly on the hour
	monday.setDate(diff);	
	monday.setHours(0);
	monday.setMinutes(0);
	monday.setSeconds(0);

	for (var i = 0; i < allDays.length; i++) { //loops through all the days
		if (String(monday) == String(new Date(allDays[i].date))) { //if the date of the current monday matches a database value
			//add the days for the current week to the list
			usefulDays1.push(allDays[i+0]);
			usefulDays1.push(allDays[i+1]);
			usefulDays1.push(allDays[i+2]);
			usefulDays1.push(allDays[i+3]);
			usefulDays1.push(allDays[i+4]);

			//add the days for the next week, if they exist
			if (allDays[i+5] != null) {
				usefulDays2.push(allDays[i+5]);
			}
			if (allDays[i+6] != null) {
				usefulDays2.push(allDays[i+6]);
			}
			if (allDays[i+7] != null) {
				usefulDays2.push(allDays[i+7]);
			}
			if (allDays[i+8] != null) {
				usefulDays2.push(allDays[i+8]);
			}
			if (allDays[i+9] != null) {
				usefulDays2.push(allDays[i+9]);
			}

			//add the days for the week after next week, if they exist
			if (allDays[i+10] != null) {
				usefulDays3.push(allDays[i+10]);
			}
			if (allDays[i+11] != null) {
				usefulDays3.push(allDays[i+11]);
			}
			if (allDays[i+12] != null) {
				usefulDays3.push(allDays[i+12]);
			}
			if (allDays[i+13] != null) {
				usefulDays3.push(allDays[i+13]);
			}
			if (allDays[i+14] != null) {
				usefulDays3.push(allDays[i+14]);
			}

			break; //stop the loop when one is found
		}
	}

	//initialises all of the variables for the tables.
	var tables = {};
	tables.c1tables = {};
	tables.c2tables = {};
	tables.c3tables = {};
	tables.c4tables = {};
	tables.c5tables = {};
	tables.c6tables = {};
	tables.c7tables = {};
	tables.c8tables = {};
	tables.c9tables = {};
	tables.c10tables = {};
	tables.c11tables = {};
	tables.c12tables = {};

	//creates all of the tables for this week
	if (usefulDays1.length > 0) {
		tables.c1tables.table1 = createTable(usefulDays1, 'C1', username, password);
		tables.c2tables.table1 = createTable(usefulDays1, 'C2', username, password);
		tables.c3tables.table1 = createTable(usefulDays1, 'C3', username, password);
		tables.c4tables.table1 = createTable(usefulDays1, 'C4', username, password);
		tables.c5tables.table1 = createTable(usefulDays1, 'C5', username, password);
		tables.c6tables.table1 = createTable(usefulDays1, 'C6', username, password);
		tables.c7tables.table1 = createTable(usefulDays1, 'C7', username, password);
		tables.c8tables.table1 = createTable(usefulDays1, 'C8', username, password);
		tables.c9tables.table1 = createTable(usefulDays1, 'C9', username, password);
		tables.c10tables.table1 = createTable(usefulDays1, 'C10', username, password);
		tables.c11tables.table1 = createTable(usefulDays1, 'C11', username, password);
		tables.c12tables.table1 = createTable(usefulDays1, 'C12', username, password);
	}
	//creates all of the tables for next week
	if (usefulDays2.length > 0) {
		tables.c1tables.table2 = createTable(usefulDays2, 'C1', username, password);
		tables.c2tables.table2 = createTable(usefulDays2, 'C2', username, password);
		tables.c3tables.table2 = createTable(usefulDays2, 'C3', username, password);
		tables.c4tables.table2 = createTable(usefulDays2, 'C4', username, password);
		tables.c5tables.table2 = createTable(usefulDays2, 'C5', username, password);
		tables.c6tables.table2 = createTable(usefulDays2, 'C6', username, password);
		tables.c7tables.table2 = createTable(usefulDays2, 'C7', username, password);
		tables.c8tables.table2 = createTable(usefulDays2, 'C8', username, password);
		tables.c9tables.table2 = createTable(usefulDays2, 'C9', username, password);
		tables.c10tables.table2 = createTable(usefulDays2, 'C10', username, password);
		tables.c11tables.table2 = createTable(usefulDays2, 'C11', username, password);
		tables.c12tables.table2 = createTable(usefulDays2, 'C12', username, password);
	}
	//creates all of the tables for the week after next
	if (usefulDays3.length > 0) {
		tables.c1tables.table3 = createTable(usefulDays3, 'C1', username, password);
		tables.c2tables.table3 = createTable(usefulDays3, 'C2', username, password);
		tables.c3tables.table3 = createTable(usefulDays3, 'C3', username, password);
		tables.c4tables.table3 = createTable(usefulDays3, 'C4', username, password);
		tables.c5tables.table3 = createTable(usefulDays3, 'C5', username, password);
		tables.c6tables.table3 = createTable(usefulDays3, 'C6', username, password);
		tables.c7tables.table3 = createTable(usefulDays3, 'C7', username, password);
		tables.c8tables.table3 = createTable(usefulDays3, 'C8', username, password);
		tables.c9tables.table3 = createTable(usefulDays3, 'C9', username, password);
		tables.c10tables.table3 = createTable(usefulDays3, 'C10', username, password);
		tables.c11tables.table3 = createTable(usefulDays3, 'C11', username, password);
		tables.c12tables.table3 = createTable(usefulDays3, 'C12', username, password);
	}

	return tables;
}

//creates the HTML for the tables
createTable = function(days, room, username, password) {
	//opens the table element, and adds the heading row
	var table = '';
		table += '<table style="background-color: white">';
		table += headingRow();

	//loops through the days of the week
	for (var i = 0; i < days.length; i++) {
		table += "<tr>"; //creates a row

		table += dayOfTheWeek(i); //adds the day label

		// for period1 bookings
		var bookingsForP1 = [];
		for (var j = 0; j < days[i].p1Bookings.length; j++) {
			if (days[i].p1Bookings[j].room == room) {
				bookingsForP1.push(days[i].p1Bookings[j]);
			}
		}

		if (bookingsForP1.length == 1) { // set or approved

			if (bookingsForP1[0].setBooking == 1) {
				if (username == 'guest') {
					table += guest_setbooking(bookingsForP1[0].staffCode, bookingsForP1[0].yearGroup);
				}	
				else {
					table += setbooking();
				}
			}
			else if (bookingsForP1[0].approved == 1) {
				if (username == 'guest') {
					table += guest_authorizedbooking(bookingsForP1[0].staffCode, bookingsForP1[0].yearGroup);
				}	
				else {
					table += authorizedbooking();
				}
			}
			else {
				if (username == 'guest') {
					table += guest_availablebooking();
				}	
				else {
					table += availablebooking(bookingsForP1.length, username, password, days[i].date, 1, room);
				}
			}
		}
		else { // open booking
			if (username == 'guest') {
				table += guest_availablebooking();
			}	
			else {
				table += availablebooking(bookingsForP1.length, username, password, days[i].date, 1, room);
			}
		}
		//end period1 bookings

		// for period2 bookings
		var bookingsForP2 = [];
		for (var j = 0; j < days[i].p2Bookings.length; j++) {
			if (days[i].p2Bookings[j].room == room) {
				bookingsForP2.push(days[i].p2Bookings[j]);
			}
		}

		if (bookingsForP2.length == 1) { // set or approved
			if (bookingsForP2[0].setBooking == 1) {
				if (username == 'guest') {
					table += guest_setbooking(bookingsForP2[0].staffCode, bookingsForP2[0].yearGroup);
				}
				else {
					table += setbooking();
				}
			}
			else if (bookingsForP2[0].approved == 1) {
				if (username == 'guest') {
					table += guest_authorizedbooking(bookingsForP2[0].staffCode, bookingsForP2[0].yearGroup);
				}
				else {
					table += authorizedbooking();
				}
			}
			else {
				if (username == 'guest') {
					table += guest_availablebooking();
				}
				else {
					table += availablebooking(bookingsForP2.length, username, password, days[i].date, 2, room);
				}
			}
		}
		else { // open booking
			if (username == 'guest') {
				table += guest_availablebooking();
			}
			else {
				table += availablebooking(bookingsForP2.length, username, password, days[i].date, 2, room);
			}
		}
		//end period2 bookings

		// for period3 bookings
		var bookingsForP3 = [];
		for (var j = 0; j < days[i].p3Bookings.length; j++) {
			if (days[i].p3Bookings[j].room == room) {
				bookingsForP3.push(days[i].p3Bookings[j]);
			}
		}

		if (bookingsForP3.length == 1) { // set or approved
			if (bookingsForP3[0].setBooking == 1) {
				if (username == 'guest') {
					table += guest_setbooking(bookingsForP3[0].staffCode, bookingsForP3[0].yearGroup);
				}
				else {
					table += setbooking();
				}
			}
			else if (bookingsForP3[0].approved == 1) {
				if (username == 'guest') {
					table += guest_authorizedbooking(bookingsForP3[0].staffCode, bookingsForP3[0].yearGroup);
				}
				else {
					table += authorizedbooking();
				}
			}
			else {
				if (username == 'guest') {
					table += guest_availablebooking();
				}
				else {
					table += availablebooking(bookingsForP3.length, username, password, days[i].date, 3, room);
				}
			}
		}
		else { // open booking
			if (username == 'guest') {
				table += guest_availablebooking();
			}
			else {
				table += availablebooking(bookingsForP3.length, username, password, days[i].date, 3, room);
			}
		}
		//end period3 bookings

		// for period4 bookings
		var bookingsForP4 = [];
		for (var j = 0; j < days[i].p4Bookings.length; j++) {
			if (days[i].p4Bookings[j].room == room) {
				bookingsForP4.push(days[i].p4Bookings[j]);
			}
		}

		if (bookingsForP4.length == 1) { // set or approved
			if (bookingsForP4[0].setBooking == 1) {
				if (username == 'guest') {
					table += guest_setbooking(bookingsForP4[0].staffCode, bookingsForP4[0].yearGroup);
				}
				else {
					table += setbooking();
				}
			}
			else if (bookingsForP4[0].approved == 1) {
				if (username == 'guest') {
					table += guest_authorizedbooking(bookingsForP4[0].staffCode, bookingsForP4[0].yearGroup);
				}
				else {
					table += authorizedbooking();
				}
			}
			else {
				if (username == 'guest') {
					table += guest_availablebooking();
				}
				else {
					table += availablebooking(bookingsForP4.length, username, password, days[i].date, 4, room);
				}
			}
		}
		else { // open booking
			if (username == 'guest') {
				table += guest_availablebooking();
			}
			else {
				table += availablebooking(bookingsForP4.length, username, password, days[i].date, 4, room);
			}
		}
		//end period4 bookings

		// for period5 bookings
		var bookingsForP5 = [];
		for (var j = 0; j < days[i].p5Bookings.length; j++) {
			if (days[i].p5Bookings[j].room == room) {
				bookingsForP5.push(days[i].p5Bookings[j]);
			}
		}

		if (bookingsForP5.length == 1) { // set or approved
			if (bookingsForP5[0].setBooking == 1) {
				if (username == 'guest') {
					table += guest_setbooking(bookingsForP5[0].staffCode, bookingsForP5[0].yearGroup);
				}
				else {
					table += setbooking();
				}
			}
			else if (bookingsForP5[0].approved == 1) {
				if (username == 'guest') {
					table += guest_authorizedbooking(bookingsForP5[0].staffCode, bookingsForP5[0].yearGroup);
				}
				else {
					table += authorizedbooking();
				}
			}
			else {
				if (username == 'guest') {
					table += guest_availablebooking();
				}
				else {
					table += availablebooking(bookingsForP5.length, username, password, days[i].date, 5, room);
				}
			}
		}
		else { // open booking
			if (username == 'guest') {
				table += guest_availablebooking();
			}
			else {
				table += availablebooking(bookingsForP5.length, username, password, days[i].date, 5, room);
			}
		}
		//end period5 bookings

		
		table += "</tr>"; //closes the table row
	}

	table += '</table>'; //closes the table

	return table;
}

//creates the heading row
headingRow = function() {
	var string = "<tr>";
		string += "<td style='background-color: white'>Day</td>";
		string += "<td style='background-color: white'>Period 1</td>";
		string += "<td style='background-color: white'>Period 2</td>";
		string += "<td style='background-color: white'>Period 3</td>";
		string += "<td style='background-color: white'>Period 4</td>";
		string += "<td style='background-color: white'>Period 5</td>";
		string += "</tr>";

	return string; //returns the HTML
}

//creates the authorized view
authorizedbooking = function() {
	var string = "<td style='background-color: yellow '>";
		string +="Booking already <br> authorised.";
		string += "</td>";

	return string; //returns the HTML
}
//creates the available booking view
availablebooking = function(bookingsMade, username, password, date, period, room) {
	var string = "<td style='background-color: green'>";
		string += "Room available! <br>";
		string += "Requests made: " + bookingsMade + " <br>";
		string += "<form action='/bookRoom' method='post'>";
		string += "<input type='hidden' name='user' value=" + username + ">";
		string += "<input type='hidden' name='pass' value=" + password +">";
		string += "<input type='hidden' name='date' value=" + date + ">";
		string += "<input type='hidden' name='period' value=" + period + ">";
		string += "<input type='hidden' name='room' value=" + room + ">";
		string += "<select name='yeargroup'>";
		string += "<option value='7'>Year 7</option>";
		string += "<option value='8'>Year 8</option>";
		string += "<option value='9'>Year 9</option>";
		string += "<option value='10'>Year 10</option>";
		string += "<option value='11'>Year 11</option>";
		string += "<option value='12'>Year 12</option>";
		string += "<option value='13'>Year 13</option>";
		string += "</select>";

		string += "<input type='submit' value='Book'>";
		string += "</form>";
		string += "</td>";

	return string; //returns the HTML
}
//creates the set booking view
setbooking = function() {
	var string = "<td style='background-color: red'>Set booking</td>"

	return string; //returns the HTML
}
//creates the authorized view for the guest booking
guest_authorizedbooking = function(staffUser, yearGroup) {
	var string = "<td style='background-color: white'>Booking made by " + staffUser + " for year " + yearGroup + "</td>"

	return string; //returns the HTML
}
//creates the available view for the guest booking
guest_availablebooking = function() {
	var string = "<td style='background-color: white'>No Lesson Booked</td>"

	return string; //returns the HTML
}
//creates the set booking view for the guest booking
guest_setbooking = function(staffUser, yearGroup) {
	var string = "<td style='background-color: white'>Booking made by " + staffUser + " for year " + yearGroup + "</td>"

	return string; //returns the HTML
}

//converts a number in the range 0 to 6, to the string of the day
dayOfTheWeek = function(day) {
	var string = "<td style='background-color: white'>";

	switch (day) {
		case 0:
			string += 'Monday';
			break;
		case 1:
			string += 'Tuesday';
			break;
		case 2:
			string += 'Wednesday';
			break;
		case 3:
			string += 'Thursday';
			break;
		case 4:
			string += 'Friday';
			break;
	}

	string += "</td>"

	return string; //returns the relative day
}

module.exports = createsetbookingview; //exports as a module so this code can be run by the web app