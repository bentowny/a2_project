module.exports = function(app) {
	app.post('/bookRoom', function(req, res) {
		db = require('diskdb');
		db.connect('./db', ['days']); //connect to database	

		var user = req.body.user,
			pass = req.body.pass,
			date = req.body.date,
			yearGroup = req.body.yeargroup,
			period = req.body.period;
			room = req.body.room;

		var booking = {
			room: room,
			staffCode: user, 
			yearGroup: yearGroup,
			approved: 0,
			setBooking: 0
		}

		var existingData = db.days.find({date:date});

		if (existingData.length > 0) {
			var newData = existingData[0];

			console.log('period of booking is:');
			console.log(period);

			if (period == 1) {
				newData.p1Bookings.push(booking);
				console.log('booking period 1');
			}
			else if (period == 2) {
				newData.p2Bookings.push(booking);
				console.log('booking period 2');
			}
			else if (period == 3) {
				newData.p3Bookings.push(booking);
				console.log('booking period 3');
			}
			else if (period == 4) {
				newData.p4Bookings.push(booking);
				console.log('booking period 4');
			}
			else if (period == 5) {
				newData.p5Bookings.push(booking);
				console.log('booking period 5');
			}

			db.days.update({date:date}, newData);
		}

		console.log('redirecting...');

		res.redirect('/user?username=' + user + '&password=' + pass);
	});
}